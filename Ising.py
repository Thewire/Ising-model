from __future__ import division #Allows division to give float results
import numpy as np #Used for many mathematical functions

#Function to calculate magnetism per site of a lattice configuration
def Magnetisation(lattice,n):  
    Spin=(np.sum(lattice))
    Average_Spin=Spin/(n*n) #n*n gives number of spins in lattice
    return Average_Spin
    
#Calculates Energy of a given lattice
def Energy(lattice,n,H,J):
    Energy=0
    for x in range(len(lattice)):     #for loops here go through every site in 
        for y in range(len(lattice)): #the lattice
            S0=lattice[x,y] 
            #Sn is the spin of the nearest neighbours. %n gives periodic boundary conditions
            #% is modulus operator so if site on edge of lattice is chosen, ie if x is
            #50 and n is 50 (list of length 50 has 0-49 items) the modulus operator would
            #give 0 and choose the [0] entry.
            Sn=lattice[(x-1)%n,y]+lattice[(x+1)%n,y]+lattice[x,(y-1)%n]+lattice[x,(y+1)%n]
            E=-J*(S0*Sn)-H*(S0) #Hamiltonian
            Energy+=E
    Average_Energy=Energy/(n*n)
    return Average_Energy    
        
#Calculates theoretical energy change if there was a flip
def EnergyChange(S0,Sn,H,J):
    return (2*S0*(H+J*Sn))  
    
#Main metropolis algorithm. Booleans define what tasks will be carried out 
def Ising(lattice=[],n=0,H=0,J=1,Temp=0,x=0,y=0,xr=0,yr=0,r=0,step=0,xs=0,ys=0,spinz=[],intervals=[]
             ,Random=True,Sweep=False,Fixed=False,Correlation=False,Frequency=False
             ,Energy=False):
   #Used for energy calculations later              
   Flip=False             
   
   #Default algorithm             
   if Random:
       i=np.random.randint(n)  #Picks a random number from zero to n
       j=np.random.randint(n)  
       Sn=lattice[(i-1)%n,j]+lattice[(i+1)%n,j]+lattice[i,(j-1)%n]+lattice[i,(j+1)%n]
       S0=lattice[i,j]
       dE=EnergyChange(S0,Sn,H,J)
       if dE < 0 or np.random.random() < np.exp(-dE/(Temp)) : #Flip condition
           lattice[i,j] *= -1  #Flip is succesfull, spin value changes 
           Flip=True
   
   #Sweep method that checks a specified site     
   if Sweep:
       Sn=lattice[(x-1)%n,y]+lattice[(x+1)%n,y]+lattice[x,(y-1)%n]+lattice[x,(y+1)%n]
       S0=lattice[x,y]
       dE=EnergyChange(S0,Sn,H,J)
       if dE < 0 or np.random.random() < np.exp(-dE/(Temp)) : 
           lattice[x,y] *= -1 
           Flip=True
   
   #Fixed boundaries    
   if Fixed:
        i=np.random.randint(n)
        j=np.random.randint(n)
        if i==0 and j==0:
            Sn=1+lattice[(i+1),j]+1+lattice[i,(j+1)]   
        elif i==n-1 and j==n-1:
            Sn=lattice[(i-1),j]+-1+lattice[i,(j-1)]+-1   
        elif i==n-1 and j==0:
            Sn=lattice[(i-1),j]+-1+1+lattice[i,(j+1)] 
        elif i==0 and j==n-1:
            Sn=1+lattice[(i+1),j]+lattice[i,(j-1)]+-1   
        elif i==0:
            Sn=1+lattice[(i+1),j]+lattice[i,(j-1)]+lattice[i,(j+1)]
        elif i==n-1:     
            Sn=lattice[(i-1),j]+-1+lattice[i,(j-1)]+lattice[i,(j+1)]
        elif j==0:
            Sn=lattice[(i-1),j]+lattice[(i+1),j]+1+lattice[i,(j+1)]
        elif j==n-1: 
            Sn=lattice[(i-1),j]+lattice[(i+1),j]+lattice[i,(j-1)]+-1
        else:     
            Sn=lattice[(i-1),j]+lattice[(i+1),j]+lattice[i,(j-1)]+lattice[i,(j+1)]
        S0=lattice[i,j]
        dE=EnergyChange(S0,Sn,H,J)
        if dE < 0 or np.random.random() < np.exp(-dE/(Temp)) : 
            lattice[i,j] *= -1 
            Flip=True
   
   #For correlation function
   if Correlation:
        Sn_corr=lattice[(xr-r)%n,yr]+lattice[(xr+r)%n,yr]+lattice[xr,(yr-r)%n]+lattice[xr,(yr+r)%n]
        S0_corr=lattice[xr,yr]   #Spin at point Si 
        Sr=Sn_corr/4   #Average value of Sr
        Sr0=S0_corr*(Sr) #Si*Sr
        return lattice,S0_corr,Sr0,Sr
        
   #spin frequency for given site     
   if Frequency:
        spin=lattice[xs,ys] 
        if spin != spinz[-1]:  #Checks if spin has flipped
            spinz.append(spin)
            intervals.append(step - sum(intervals))  #Records interval between last flip
        return lattice, spinz, intervals    
            
   if Energy:
        if Flip:
            Average_dE=(dE/(n*n))   #Returns change in energy if there has been a flip
        else:
            Average_dE=0
        return lattice, Average_dE    
        
   return lattice  #returns lattice   
        
       
   
        

        
       



              

    
    