from __future__ import division
import pbm    #Modules that contain useful functions
import Ising
import copy   #Used to make copies of arrays at least in my case
#import imageio   #For making video
#imageio.plugins.ffmpeg.download()
import numpy as np
import matplotlib.pylab as plt   #For plotting
import time   #For recording durations of time

#Function for making animation of lattice change
def Anim(n,steps,video_length,frames,J,Temp):    
    Initial_Array=pbm.InitialConfig(n)
    i=(steps/frames)
    writer=imageio.get_writer('{0} {1}.mp4'.format(Temp,J), mode='I', fps=frames/video_length)
    #writer=imageio.get_writer('movie.mp4', mode='I', fps=frames/video_length)
    lattice=copy.copy(Initial_Array)
    for _ in range(steps):
        lattice=Ising.Ising(lattice=lattice,n=n,J=J,Temp=Temp)
        if i == (steps/frames):  
            pbm.Video(writer,copy.copy(lattice))
            i=0
        else:   
            i+=1

#Function that compares initial lattice to lattice at end of iterations
def Comp(n,steps,J,Temp):
    Initial_Array=pbm.InitialConfig(n)
    lattice=copy.copy(Initial_Array)   #Makes copy of Array so that changing it won't change both
    for _ in range(steps):
        lattice=Ising.Ising(lattice=lattice,n=n,J=J,Temp=Temp)
    pbm.Picture(Initial_Array,lattice)    
    
 
#Calculating Magnetism,Energy,Susceptibility,Capacity   
def MESC(n,steps,J):
    t=np.arange(1.5,3,0.01)  #Temperature values
    Energy=[]
    Magnetization=[]   #Initialise many arrays
    Susceptibility=[]
    Heat=[]
    M2_list=[]
    E2_list=[]
    Initial_Array=pbm.InitialConfig(n)
    lattice=copy.copy(Initial_Array)
    for Temp in t:
        for _ in range(steps):
            for x in range(len(lattice)):    #For sweep method so that each site in the lattice is chosen seqeuentially
                for y in range(len(lattice)):
                    lattice=Ising.Ising(lattice=lattice,n=n,J=J,Temp=Temp,x=x,y=y,Sweep=True,Random=False)     
        M,E,M2,E2=MESC2(lattice,n,int(steps/2),0,1,Temp)
        Energy.append(E)
        Magnetization.append(M)
        E2_list.append(E2)
        M2_list.append(M2)
    for i in range (len(Magnetization)):    
        Susceptibility.append((1/t[i])*(M2_list[i]-(Magnetization[i])**2))   
        Heat.append((1/(t[i]**2))*(E2_list[i]-(Energy[i])**2))   
    pbm.MESC(t,Magnetization,Energy,Susceptibility,Heat)
    return t,Magnetization,Energy,Susceptibility,Heat

#Mesc part2 to make things neater    
def MESC2(lattice,n,steps,J,Temp):
    Magnetization=0
    M2=0
    Energy=0
    E2=0
    Equ=(Ising.Energy(lattice,n,0,J))
    for _ in range(steps):
        for x in range(len(lattice)):
                for y in range(len(lattice)):
                    Var=Ising.Ising(lattice=lattice,n=n,J=J,Temp=Temp,x=x,y=y,Sweep=True,Energy=True,Random=False)
                    lattice=Var[0]
                    dM=abs((Ising.Magnetisation(lattice,n)))
                    dE=Var[1]
                    Equ+=dE
                    Magnetization+=dM
                    Energy+=Equ
                    M2+=(dM)**2
                    E2+=(Equ)**2 
    return Magnetization/(steps*n*n), Energy/(steps*n*n) , M2/(steps*n*n), E2/(steps*n*n)    

#Makes graph of iterations versus magnetism to show equilibrium being reached   
def equilibrium(Temp,steps,n):
    Initial_Array=pbm.InitialConfig(n)
    step=[]
    Magnet=[]
    lattice=copy.copy(Initial_Array)
    for i in range(steps):
        lattice=Ising.Ising(lattice=lattice,n=n,Temp=Temp) 
        step.append(i)
        Magnet.append(abs(Ising.Magnetisation(lattice,n)))
    plt.ylabel('Average Magnetisation per spin')
    plt.xlabel('Iterations')      
    plt.plot(step,Magnet)     

#equilibrium comparison between sweep and default methods            
def sweepVsRan(Temp,steps,n):
    Initial_Array=pbm.InitialConfig(n)
    i=0
    step=[]
    Magnet=[]
    lattice=copy.copy(Initial_Array)
    start = time.time() #*
    for _ in range(steps):
          for x in range(len(lattice)):
               for y in range(len(lattice)):
                   lattice=Ising.Ising(lattice=lattice,n=n,Temp=Temp,x=x,y=y,Sweep=True,Random=False)
                   step.append(i)
                   Magnet.append(abs(Ising.Magnetisation(lattice,n)))
                   i+=1          
    end = time.time()#*
    print 'Sweep time' ,(end - start),  #Shows time takes to go between the two spots shown as #* above          
    plt.plot(step,Magnet,label="Sweep")  
    step=[]
    Magnet=[]
    lattice=copy.copy(Initial_Array)
    start = time.time()
    for i in range((steps*n*n)):
        lattice=Ising.Ising(lattice=lattice,n=n,Temp=Temp) 
        step.append(i)      
        Magnet.append(abs(Ising.Magnetisation(lattice,n))) 
    end = time.time()
    print 'Random time' ,(end - start),           
    plt.plot(step,Magnet,label="Random")    
    plt.legend(loc=4)
    plt.ylabel('Average Magnetisation per spin')
    plt.xlabel('Iterations') 
    plt.show()

#Function for correlation function    
def Correlation(n,steps):
    Var=[]
    Initial_Array=pbm.InitialConfig(n)
    Temp=[1.5,2.269,3.5]
    rar=[0,1,2,3,4,5,6,7,8,9,10]  #r values
    xr=int(n/2)  #specifies center point as Si
    yr=int(n/2)
    lattice=copy.copy(Initial_Array)
    for Temp in Temp:
        corr=[]
        for r in rar:
            print r
            S0_corr=0
            Sr0=0
            Sr=0
            for _ in range(steps):
                lattice=Ising.Ising(lattice=lattice,n=n,Temp=Temp)
            for _ in range(int(steps/2)):
                Var=Ising.Ising(Temp=Temp,r=r,xr=xr,yr=yr,n=n,lattice=lattice,Correlation=True)
                lattice=Var[0]
                S0_corr+=Var[1]
                Sr0+=Var[2]
                Sr=Var[3]  
            S0_corr=S0_corr/(steps/2)  
            Sr0=Sr0/(steps/2)
            Sr=Sr/(steps/2)
            func=np.abs(Sr0-(S0_corr)*(Sr))
            corr.append(func)
        filename = "%s.csv" % Temp
        np.savetxt(filename, corr, delimiter=",")       
        plt.plot(rar, corr, 'o-', label='Temp = {0}'.format(Temp))
    plt.xlabel('r')
    plt.ylabel('Correlation Function')
    plt.legend()
    plt.show()    

#Shows spin frequency at different temperatures            
def Frequency(n,steps): 
     Initial_Array=pbm.InitialConfig(n)  
     Temp=[1.5,2.2,2.5,5]     
     xs=int(n/2)
     ys=int(n/2)
     lattice=copy.copy(Initial_Array)
     for i, Temp in enumerate(Temp): #enumerate makes counter
         for _ in range(steps):
                lattice=Ising.Ising(lattice=lattice,n=n,Temp=Temp)
         spinz=[lattice[xs,ys]]
         intervals=[]       
         for step in range(int(steps/2)):
             lattice,spinz,intervals=Ising.Ising(Temp=Temp,xs=xs,ys=ys,n=n,lattice=lattice,Frequency=True,step=step,spinz=spinz,intervals=intervals)
         plt.subplot(3,2,i+1)   #Changes subplot size based on how many graphs there are
         plt.hist(intervals, 50)  #plots histogram
         plt.locator_params(nbins=4) 
         plt.title('Temp = {0}'.format(Temp))
         plt.xlabel('Flip Interval Frequency')
         plt.ylabel('No. of occurances')
     plt.tight_layout()
     plt.show()   

#Function for showing how magnetism and energy changes with external magnetic field         
def Field(n,steps,Temp):
    Initial_Array=pbm.InitialConfig(n)
    Magnetization=[]
    Energy=[]
    B1=np.arange(-2,2,.05)
    B2=B1*(-1)
    lattice=copy.copy(Initial_Array)
    for H in B1:
        mag=0
        E=0
        for _ in range(steps):
             lattice=Ising.Ising(lattice=lattice,n=n,H=H,Temp=Temp) 
        for _ in range(int(steps/2)):
             lattice=Ising.Ising(lattice=lattice,n=n,H=H,Temp=Temp) 
             dM=(Ising.Magnetisation(lattice,n))
             dE=(Ising.Energy(lattice,n,H,1))
             mag+=dM
             E+=dE
        Magnetization.append(mag/(steps/2))
        Energy.append(E/(steps/2))
    Magnetization1=[]
    Energy1=[]
    for H in B2:
        mag=0
        E=0
        for _ in range(steps):
             lattice=Ising.Ising(lattice=lattice,n=n,H=H,Temp=Temp)   
        for _ in range(int(steps/2)):
             lattice=Ising.Ising(lattice=lattice,n=n,H=H,Temp=Temp)
             dM=(Ising.Magnetisation(lattice,n))
             dE=(Ising.Energy(lattice,n,H,1))
             mag+=dM
             E+=dE
        Magnetization1.append(mag/(steps/2))
        Energy1.append(E/(steps/2))   
    pbm.Field(B1,B2,Magnetization,Energy,Magnetization1,Energy1)

#Equilibrium test for several starting lattices
def initial(n,steps,Temp):
    Initial_Array=pbm.InitialConfig(n)
    lattice=copy.copy(Initial_Array)
    step=[]
    Magnet=[]
    start = time.time()
    for i in range(steps):
        lattice=Ising.Ising(lattice=lattice,n=n,Temp=Temp)
        step.append(i)      
        Magnet.append(abs(Ising.Magnetisation(lattice,n))) 
    end = time.time()
    print 'Random' ,(end - start),
    plt.plot(step,Magnet,label="Random") 

    Initial_Array1=pbm.Initial1(n)
    lattice1=copy.copy(Initial_Array1)
    step=[]
    Magnet=[]
    start = time.time()
    for i in range(steps):
        lattice=Ising.Ising(lattice1=lattice,n=n,Temp=Temp)
        step.append(i)      
        Magnet.append(abs(Ising.Magnetisation(lattice1,n))) 
    end = time.time()
    print 'Lines' ,(end - start)  ,
    plt.plot(step,Magnet,label="Lines")

    Initial_Array2=pbm.Initial2(n)
    lattice2=copy.copy(Initial_Array2)
    step=[]
    Magnet=[]
    start = time.time()
    for i in range(steps):
       lattice=Ising.Ising(lattice2=lattice,n=n,Temp=Temp)
       step.append(i)      
       Magnet.append(abs(Ising.Magnetisation(lattice2,n))) 
    end = time.time()
    print 'Half' ,(end - start),
    plt.plot(step,Magnet,label="Half")
   
    Initial_Array3=pbm.Initial3(n)
    lattice3=copy.copy(Initial_Array3)
    step=[]
    Magnet=[]
    start = time.time()
    for i in range(steps):
        lattice=Ising.Ising(lattice3=lattice,n=n,Temp=Temp)
        step.append(i)      
        Magnet.append(abs(Ising.Magnetisation(lattice3,n))) 
    end = time.time()
    print 'Alternate' ,(end - start),
    plt.plot(step,Magnet,label="Alt")
    plt.legend(loc=4)
    plt.show()
    
    
      
             
            
#Anim(112,25000000,10,1000,1,1.4)  
#Comp(50,6000000,1,5)          
#equilibrium(1.8,300,96)    
#sweepVsRan(1.8,700,50)
#Correlation(80,8000000)
#Frequency(20,1000000)
#Field(20,10000,1.5)
#initial(20,80000,0.1)
#sizes=[10,20,30,40,50,100]   #Does MESC calculations for several lattice sizes and saves data as .csv
#for size in sizes:
 #   n=size
  #  x=MagVsT(n,300*n)
   # filename = "%s.csv" % n
    #np.savetxt(filename, x, delimiter=",")   
    


           
