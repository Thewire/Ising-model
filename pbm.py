from __future__ import division
import numpy as np
import matplotlib
import matplotlib.pylab as plt
#import imageio
#imageio.plugins.ffmpeg.download()

#Initial random array
def InitialConfig(n):
    A=np.random.choice([1, -1], size=(n, n))
    return A

#Array of alternating lines  
def Initial1(n):
    A=np.zeros((n,n))
    for j in range(n):
        i=0
        while i < n:
            A[i][j]=1
            i+=1
            A[i][j]=-1
            i+=1
    return A        

#Array of half either spin    
def Initial2(n):
    A=np.full((n,n),1)
    for j in range(n):
        for i in range(int((n)/2)):
            A[i][j]=-1
    return A       

#Array of alternating spins     
def Initial3(n):
    A=np.zeros((n,n))
    j=0
    i=0
    while j < n:
        i=0
        while i < n:
            A[i][j]=1
            i+=1
            A[i][j]=-1
            i+=1
        i=0    
        j+=1
        while i < n:
            A[i][j]=-1
            i+=1
            A[i][j]=1
            i+=1
        j+=1    
    return A      

#Appends .png of lattice to video    
def Video(writer,A):
   matplotlib.image.imsave('A.png', A)
   writer.append_data(imageio.imread('A.png'))

#Makes plot of two arrays passed to it    
def Picture(A,B):
   plt.figure(1)
   plt.subplot(211)
   plt.imshow(A)
   
   plt.subplot(212)
   plt.imshow(B)
   plt.show()

#Used for MESC plots for neatness    
def MESC(t,Magnetization,Energy,Suscebility,Heat):
    f1 = plt.figure()
    f2 = plt.figure()
    f3 = plt.figure()
    f4 = plt.figure()
    ax1 = f1.add_subplot(111)
    ax1.plot(t,Magnetization)
    ax1.set_ylabel('Average magnetisation per site (u)')
    ax1.set_xlabel('Temperature (J/KB)')
    ax2 = f2.add_subplot(111)
    ax2.plot(t,Energy)
    ax2.set_ylabel('Average energy per site (J)')
    ax2.set_xlabel('Temperature (J/KB)')
    ax3 = f3.add_subplot(111)
    ax3.plot(t,Suscebility)
    ax3.set_ylabel('Magnetic susceptibility (u/kB)')
    ax3.set_xlabel('Temperature (J/KB)')
    ax4 = f4.add_subplot(111)
    ax4.plot(t,Heat)
    ax4.set_ylabel('Specific Heat (J/kB^2)')
    ax4.set_xlabel('Temperature (J/KB)')
    plt.show()

#Used for Field plots for neatness    
def Field(B1,B2,Magnetization,Energy,Magnetization1,Energy1):
    f1 = plt.figure()
    f2 = plt.figure()    
    ax1 = f1.add_subplot(111)
    ax2 = f2.add_subplot(111)
    ax1.plot(B1,Magnetization,'o-',label='Forward Change')
    ax1.plot(B2,Magnetization1,'o-',label='Backward Change')
    ax2.plot(B1,Energy,'o-',label='Forward Change') 
    ax2.plot(B2,Energy1,'o-',label='Backward Change') 
    ax1.set_xlabel('External Magnetic Field (T)')
    ax2.set_xlabel('External Magnetic Field (T)')
    ax1.set_ylabel('Average Magnetism per spin')
    ax2.set_ylabel('Average Energy per spin')
    box = ax1.get_position()
    ax1.set_position([box.x0, box.y0, box.width * 0.8, box.height]) 
    ax1.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    box = ax2.get_position()
    ax2.set_position([box.x0, box.y0, box.width * 0.8, box.height])
    ax2.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.show()    
   

   

